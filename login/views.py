from django.shortcuts import render
#from .forms import UserForm,UserProfileInfoForm
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from .forms import LoginForm


# Create your views here.

def userLogin(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request,user)

                return HttpResponseRedirect(reverse('index'))
            else:
                message = "User account is inactive"
                return HttpResponse(message)
        else:
            message = "Invalid login credentials"
            return HttpResponse(message)
    else:
        message = "User does not exist"
        return HttpResponse(message)

    form=LoginForm()
    return render(request,'login/login.html',
                          {'LoginForm':form})
