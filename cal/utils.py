from datetime import datetime, timedelta
from calendar import HTMLCalendar
from django.contrib import messages
from .models import Event


class Calendar(HTMLCalendar):
	def __init__(self, year=None, month=None):
		self.year = year
		self.month = month
		super(Calendar, self).__init__()

	# formats a day as a td
	# filter events by day
	def formatday(self, day, events, user):
		print(user)
		events_per_day = events.filter(day__day=day, uzer=user)
		alert_user=False
		d = ''
		for event in events_per_day:
			d+=f'<li>{event.get_html_url}</li>'
			if(day==datetime.now().day):
				alert_user=True

		if day != 0:
			if alert_user==False or self.year!=datetime.now().year or self.month != datetime.now().month:
				return f"<td><span class='date'>{day}</span><ul>{d}</ul></td>"
			else:
				return f"<td bgcolor='#ff9999'><span class='date'>{day}</span><ul>{d}</ul></td>"
		return '<td></td>'

		

	# formats a week as a tr
	def formatweek(self, theweek, events, user):
		week = ''
		for d, weekday in theweek:
			week += self.formatday(d, events, user)
		return f'<tr> {week} </tr>'

	# formats a month as a table
	# filter events by year and month
	def formatmonth(self, withyear=True, user=''):
		events = Event.objects.filter(day__year=self.year, day__month=self.month)
		self.user=user
		cal = f'<table border="0" cellpadding="0" cellspacing="0" class="calendar">\n'
		cal += f'{self.formatmonthname(self.year, self.month, withyear=withyear)}\n'
		cal += f'{self.formatweekheader()}\n'
		for week in self.monthdays2calendar(self.year, self.month):
			cal += f'{self.formatweek(week, events, user)}\n'
		return cal
