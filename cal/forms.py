from django.forms import ModelForm, DateInput, Textarea
from cal.models import Event

class EventForm(ModelForm):
  class Meta:
    model = Event
    # exclude = ('uzer',)
    # datetime-local is a HTML5 input type, format to make date time show on fields
    widgets = {
      'day': DateInput(attrs={'type': 'date'}, format='%Y-%m-%d'),
      'start_time': DateInput(attrs={'type': 'datetime-local'}, format='%Y-%m-%dT%H:%M'),
      'end_time': DateInput(attrs={'type': 'datetime-local'}, format='%Y-%m-%dT%H:%M'),
      'description' : Textarea(attrs={'required': False})
    }
    fields = '__all__'

  def __init__(self, *args, **kwargs):
    super(EventForm, self).__init__(*args, **kwargs)
    # input_formats parses HTML5 datetime-local input to datetime field
    self.fields['day'].input_formats = ('%Y-%m-%d',)
    self.fields['start_time'].input_formats = ('%Y-%m-%dT%H:%M',)
    self.fields['end_time'].input_formats = ('%Y-%m-%dT%H:%M',)
    self.fields['uzer'].widget.attrs['hidden']=True