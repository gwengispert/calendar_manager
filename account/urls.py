from django.conf.urls import url
from account import views
from django.urls import path

app_name = 'account'

urlpatterns=[
	path('', views.index, name='index'),
	url(r'^index/$', views.index, name='index'),
    url(r'^register/$',views.register,name='register'),
	url(r'^login/$',views.loginProcess,name='login'),
	url(r'^loginprocess/$',views.loginProcess,name='loginProcess'),
	url(r'^logout/$', views.logoutProcess, name='logoutProcess'),
]
