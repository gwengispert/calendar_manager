from django.shortcuts import render
from account.forms import UserForm#,UserProfileInfo
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from login.forms import LoginForm
# from django.contrib.auth.decorators import login_required

# Create your views here.

def logoutProcess(request):
    logout(request)
    return HttpResponseRedirect("../index")

def index(request):
    return render(request,'index.html')

def register(request):
    registered = False
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)
       # profile_form = UserProfileInfoForm(data=request.POST)
        if user_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            #profile = profile_form.save(commit=False)
            #profile.user = user
            #if 'profile_pic' in request.FILES:
                #print('found it')
                #profile.profile_pic = request.FILES['profile_pic']
            #profile.save()
            registered = True
        else:
            print(user_form.errors) #,profile_form.errors
    else:
        user_form = UserForm()
        #profile_form = UserProfileInfo()
    return render(request,'registration.html',
                          {'user_form':user_form,
                           #'profile_form':profile_form,
                           'registered':registered})

# def login(request):
#     return render(request, 'login/login.html')

def loginProcess(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request,user)
                request.session['usr']=request.user.id
                print(request.user.id)
                print(request.session['usr'])
                return HttpResponseRedirect('..')
            else:
                message = "User account is inactive"
                return HttpResponse(message)
        else:
            message = "Invalid login credentials"
            return HttpResponse(message)
    # else:
    #     message = "User does not exist"
    #     return HttpResponse(message)
    form=LoginForm()
    return render(request,'login.html',
                          {'LoginForm':form})                        
