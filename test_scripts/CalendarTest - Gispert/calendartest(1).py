from selenium import webdriver
from selenium.webdriver.common.keys import Keys

driver = webdriver.Chrome()
driver.get("http://localhost:8000/")

# Login
toLogin = driver.find_element_by_link_text('Login')
driver.save_screenshot('Calendar Test - 1.1.png')
toLogin.click()

# Insert credentials
usernameInput = driver.find_element_by_name('username')
usernameInput.send_keys('admin')
passwordInput = driver.find_element_by_name('password')
passwordInput.send_keys('admin@123')
driver.save_screenshot('Calendar Test - 1.2.png')
passwordInput.send_keys(Keys.RETURN)

# Navigate to calendar
toCalendar = driver.find_element_by_link_text('here')
driver.save_screenshot('Calendar Test - 1.3.png')
toCalendar.click()


# Click on invalid event
invalidEvent = driver.find_element_by_class_name("date")
driver.save_screenshot('Calendar Test - 1.4.png')
invalidEvent.click()

driver.save_screenshot('Calendar Test - 1.5.png')

