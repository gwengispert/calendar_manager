from selenium import webdriver
from selenium.webdriver.common.keys import Keys

driver = webdriver.Chrome()
driver.get("http://localhost:8000/")


# Login 
toLogin = driver.find_element_by_link_text('Login')
driver.save_screenshot('Calendar Test - 5.1.png')
toLogin.click()


# Input credentials
usernameInput = driver.find_element_by_name('username')
usernameInput.send_keys('admin')
passwordInput = driver.find_element_by_name('password')
passwordInput.send_keys('admin@123')
driver.save_screenshot('Calendar Test - 5.2.png')
passwordInput.send_keys(Keys.RETURN)

# Navigate to calendar
toCalendar = driver.find_element_by_link_text('here')
driver.save_screenshot('Calendar Test - 5.3.png')
toCalendar.click()

# Click New Event button
toNewEvent = driver.find_element_by_link_text('New Event')
driver.save_screenshot('Calendar Test - 5.4.png')
toNewEvent.click()

driver.save_screenshot('Calendar Test - 5.5.png')