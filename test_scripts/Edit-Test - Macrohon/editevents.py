from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

dateToday="25-05-2019"
def goToCalendar(failedOnce=False, prefix="", username="1234", password="1234"):
    driver.get("http://localhost:8000/")
    calendarGoTo=''
    try:
        calendarGoTo=driver.find_element_by_link_text("here")
        calendarGoTo.click()
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        takeScreenshot("00 - Starting Calendar", prefix)
    except:
        try:
            if(failedOnce==False):
                toLogin=driver.find_element_by_link_text("Login")
                toLogin.click()
                userInput=driver.find_element_by_name('username')
                userInput.send_keys(username)
                passwordInput=driver.find_element_by_name('password')
                passwordInput.send_keys(password)
                passwordInput.send_keys(Keys.RETURN)
                goToCalendar(True)
            else:
                print("Failed twice. Aborting")
        except:
            print("Failed to find login. Aborting")
def logout():
    driver.get("http://localhost:8000/")
    logoutGoTo=''
    try:
        logoutGoTo=driver.find_element_by_link_text("log out")
        logoutGoTo.click()
    except:
        print("Already logged out")

def goToNewEvent():
    goToCalendar()
    newEvent=driver.find_element_by_link_text("New Event")
    newEvent.click()
def goToEvent(title):
    goToCalendar()
    event=driver.find_element_by_link_text(title)
    event.click()
def takeScreenshot(screenshotName, prefix=""):
    if(prefix!="" and screenshotName!=""):
        driver.save_screenshot(prefix+"-"+screenshotName+'.png')
        # driver.save_screenshot("test.pg")
        print("Photo Taken")
def makeNewEvent(day, title, description, start_time, end_time, prefix=""):
    goToNewEvent()
    doEventStuff(day, title, description, start_time, end_time)
def editEvent(currentTitle, day="", title="", description="", start_time="", end_time="", prefix="EditEvent"):
    goToEvent(currentTitle)
    takeScreenshot("01 - Editing", prefix)
    doEventStuff(day, title, description, start_time, end_time, prefix)
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    takeScreenshot("03 - Calendar Page", prefix)
def deleteEvent(currentTitle, prefix="DeleteEvent"):
    goToEvent(currentTitle)
    doEventStuff("", "", "", "", "", prefix, True)
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    takeScreenshot("03 - Calendar Page", prefix)

def doEventStuff(new_day, new_title, new_description, new_start_time, new_end_time, prefix="", toDelete=False):
    #ONLY Run when already in an event
    day=driver.find_element_by_name('day')
    title=driver.find_element_by_name('title')
    description=driver.find_element_by_name('description')
    startTime=driver.find_element_by_name('start_time')
    endTime=driver.find_element_by_name('end_time')
    submitButton=driver.find_element_by_name("submit_button")
    deleteButton=driver.find_element_by_name("delete_button")
    if(toDelete==False):
        if(new_day!=""):
            day.clear()
            day.send_keys(new_day)
        if(new_title!=""):
            title.clear()
            title.send_keys(new_title)
        if(new_description!=""):
            description.clear()
            description.send_keys(new_description)
        if(new_start_time!=""):
            startTime.clear()
            startTime.send_keys(new_start_time)
        if(new_end_time!=""):
            endTime.clear()
            endTime.send_keys(new_end_time)
        takeScreenshot("02 - Pre-Submit", prefix)
        submitButton.click()
    else:
        takeScreenshot("02 - Pre-Delete", prefix)
        deleteButton.click()

def doClearStuff(listOfBools, prefix=""):
    day=driver.find_element_by_name('day')
    title=driver.find_element_by_name('title')
    description=driver.find_element_by_name('description')
    startTime=driver.find_element_by_name('start_time')
    endTime=driver.find_element_by_name('end_time')
    submitButton=driver.find_element_by_name("submit_button")
    if 0 in listOfBools:
        day.clear()
    if 1 in listOfBools:
        title.clear()
    if 2 in listOfBools:
        description.clear()
    if 3 in listOfBools:
        startTime.clear()
    if 4 in listOfBools:
        endTime.clear()
    takeScreenshot("01-Pre-Submit", prefix)
    submitButton.click()

def editEventTest(username, dateToday=dateToday, prefix="EditEventTest"):
    goToCalendar(False, "", username)
    makeNewEvent(dateToday, "Selenium Test Event", "This is the unmodified description", dateToday+Keys.ARROW_RIGHT+"11-00"+Keys.ARROW_RIGHT+"AM",dateToday+Keys.RIGHT+"11-00"+Keys.ARROW_RIGHT+"PM")
    goToCalendar(False, prefix)
    editEvent("Selenium Test Event", "27-05-2019", "Test Title", "This is the modified description", dateToday+Keys.ARROW_RIGHT+"10-00"+Keys.ARROW_RIGHT+"AM", "", prefix)
    goToEvent("Test Title")
    takeScreenshot("04 - After", prefix)
    logout()
def deleteEventTest(username, dateToday=dateToday, prefix="DeleteEventTest"):
    goToCalendar(False, "", username)
    makeNewEvent(dateToday, "Selenium Delete Event", "This is the unmodified description", dateToday+Keys.ARROW_RIGHT+"11-00"+Keys.ARROW_RIGHT+"AM",dateToday+Keys.RIGHT+"11-00"+Keys.ARROW_RIGHT+"PM")
    goToCalendar(False, prefix)
    deleteEvent("Selenium Delete Event", prefix)
    logout()
def deleteManyTest(username, dateToday=dateToday, prefix="TwoEventsTest"):
    goToCalendar(False, "", username)
    makeNewEvent(dateToday, "Selenium Event is a Thing", "This is the unmodified description", dateToday+Keys.ARROW_RIGHT+"11-00"+Keys.ARROW_RIGHT+"AM",dateToday+Keys.RIGHT+"11-00"+Keys.ARROW_RIGHT+"PM")
    makeNewEvent(dateToday, "Selenium Second", "This is the unmodified description", dateToday+Keys.ARROW_RIGHT+"11-00"+Keys.ARROW_RIGHT+"AM",dateToday+Keys.RIGHT+"11-00"+Keys.ARROW_RIGHT+"PM")
    goToCalendar(False, prefix)
    deleteEvent("Selenium Second", prefix)
    takeScreenshot("04-Post Delete Screenshot", prefix)
    logout()
def clearTest(username, listOfBools, dateToday=dateToday, prefix="ClearTest"):
    goToCalendar(False, "", username)
    makeNewEvent(dateToday, "Selenium Test Event", "This is the unmodified description", dateToday+Keys.ARROW_RIGHT+"11-00"+Keys.ARROW_RIGHT+"AM",dateToday+Keys.RIGHT+"11-00"+Keys.ARROW_RIGHT+"PM")
    goToCalendar(False, prefix)
    goToEvent("Selenium Test Event")
    doClearStuff(listOfBools, prefix)
    takeScreenshot("02 - After", prefix)
    try:
        driver.find_element_by_link_text("Selenium Test Event").click()
        takeScreenshot("03 - Inside Event", prefix)
        logout()
    except:
        logout()

chrome_options = Options()
chrome_options.add_argument('--start-maximized')
chrome_options.add_argument('--start-fullscreen')
driver = webdriver.Chrome(chrome_options=chrome_options)
# editEventTest("test1", "26-05-2019", "NotToday EditEventTest")
# deleteEventTest("test2", "26-05-2019", "NotToday DeleteEventTest")
# editEventTest("test3")
# deleteEventTest("test4")
# deleteManyTest("test5")
clearTest("test6", [0])
# clearTest("test7", [2], dateToday, "ClearTestDescription")
driver.close()