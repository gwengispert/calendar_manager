from selenium import webdriver
from selenium.webdriver.common.keys import Keys

driver = webdriver.Chrome()
driver.get("http://localhost:8000/")

# Login dodes here
toLogin = driver.find_element_by_link_text('Login')
toLogin.click()

usernameInput = driver.find_element_by_name('username')
usernameInput.send_keys('1234')
passwordInput = driver.find_element_by_name('password')
passwordInput.send_keys('1234')
passwordInput.send_keys(Keys.RETURN)

# Event codes here
toCalendar = driver.find_element_by_link_text('here')
toCalendar.click()

toNewEvent = driver.find_element_by_link_text('New Event')
toNewEvent.click()

day = driver.find_element_by_name('day')
title = driver.find_element_by_name('title')
description = driver.find_element_by_name('description')
startTime = driver.find_element_by_name('start_time')
endTime = driver.find_element_by_name('end_time')
submitButton = driver.find_element_by_name("submit_button")
calendar = driver.find_element_by_link_text('event')

day.send_keys('25052019')
title.send_keys('ABALOS - Scenario 3 (no end date)')
description.send_keys('test description')
startTime.send_keys('25052019'+Keys.ARROW_RIGHT+'0730'+Keys.ARROW_RIGHT+'a')
endTime.send_keys('')

driver.save_screenshot('Scenario 3 (No End Date) - event page.jpg')

submitButton.click()
calendar.click()

driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
driver.save_screenshot('Scenario 3 (No End Date) - calendar.jpg')
