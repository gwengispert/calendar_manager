import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

        driver = webdriver.Chrome()
        driver.get("http://127.0.0.1:8000")
        driver.save_screenshot("landingpage.png")

        #Register Start
        driver.find_element_by_link_text('Register').click()
        driver.save_screenshot("registerpage.png")

        username = driver.find_element_by_id("username")
        password = driver.find_element_by_id("password")
        email = driver.find_element_by_id("email")

        #Register - No Inputs
        username.clear()
        password.clear()
        email.clear()

        email.send_keys(Keys.RETURN)
        driver.save_screenshot("registerEmpty.png")

        #Register - Username Taken
        username.send_keys("admin")
        password.send_keys("scriptpassword")
        email.send_keys("admin14441")

        email.send_keys(Keys.RETURN)
        driver.save_screenshot("registerAlreadyTakenName.png")

        username.clear()
        password.clear()
        email.clear()

        #Register - Invalid Email
        username.send_keys("scriptadmin")
        password.send_keys("scriptpassword")
        email.send_keys("scriptadmin")
        email.send_keys(Keys.RETURN)

        driver.save_screenshot("registerInvalidEmail.png")

        #Register - Success
        email.clear()
        email.send_keys("scriptadmin@gmail.com")
        email.send_keys(Keys.RETURN)

        driver.save_screenshot("registerComplete.png")

        #Moving to Login Page
        driver.find_element_by_link_text('here').click()
        driver.find_element_by_link_text('Login').click()

        #Login - Blank inputs
        password.send_keys(Keys.RETURN)
        driver.save_screenshot("loginBlank.png")

        #Login - Wrong Password
        username.send_keys("scriptadmin")
        password.send_keys("admin")
        driver.save_screenshot("loginWrongpass.png")
        password.send_keys(Keys.RETURN)
        driver.save_screenshot("loginFail.png")

        driver.back()
        username.clear()
        password.clear()

        #Login - Success
        username.send_keys("scriptadmin")
        password.send_keys("scriptpassword")
        driver.save_screenshot("passwordlogin.png")
        password.send_keys(Keys.RETURN)
        driver.save_screenshot("loginSuccessful.png")
